# Chaos

A first attempt to play with Chaos theory and bifurcation diagrams. An excuse to learn about FuncAnimation, cython and multiprocessing. You can access the notebook [directly](http://nbviewer.ipython.org/urls/git.ias.u-psud.fr/abeelen/chaos/raw/master/Chaos.ipynb).

## Intro

What happen to the simple function  
```
x_{n+1} = r * x_{n} * (1 - x_{n})
```
with a random start ```x{0}``` and different values of ```r```. Well,  this is not as simple as you might think :

* r = 2.6 : It seems that everything is converging to a single value, easy...

![r = 2.6](r_2.6.gif)

* r = 3.2 : Well here it converges to 2 different values, odds...

![r=3.2](r_3.2.gif)

* r = 3.5 : Even stranger, now there is 4 different values...

![r=3.5](r_3.5.gif)

* r = 3.6 : then now it seems that we are not converging anymore...

![r=3.6](r_3.6.gif)

## Bifucartion diagram

Now what happens to ```x_n``` when n goes to infinity (well a large number) when we change the value of ```r```. This is called a bifurcation diagram :

![bifurcation diagram](bifurcation.gif)